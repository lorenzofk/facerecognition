import cv2
import os
import re
import numpy as np
import sys
import json

model = cv2.createLBPHFaceRecognizer(1, 8, threshold=55.0)

def detect(img, cascade):
  gray = to_grayscale(img)
  rects = cascade.detectMultiScale(gray, scaleFactor=1.05, minNeighbors=5, minSize=(30, 30))

  if len(rects) == 0:
    return []
  return rects

def load_image(path):
    return cv2.imread(path, cv2.IMREAD_GRAYSCALE)

def detect_faces(img):
  cascade = cv2.CascadeClassifier("data/haarcascade_frontalface_alt2.xml")
  image = cv2.imread(img)

  return detect(image, cascade)

def to_grayscale(img):
  #gray = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)
  gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
  gray = cv2.equalizeHist(gray)
  return gray

def contains_face(img):
  return len(detect_faces(img)) > 0

def save(path, img):
  cv2.imwrite(path, img)

def crop_faces(img, faces):
  for face in faces:
    x, y, h, w = [result for result in face]
    return img[y:y+h,x:x+w]

def predict(cv_image):
  faces = detect_faces(cv_image)
  result = None
  print faces
  if len(faces) > 0:
    image = cv2.imread(cv_image)
    cropped = to_grayscale(crop_faces(image, faces))
    resized = cv2.resize(cropped, (200,200))

    images = []
    labels = []
    for path in os.listdir("/home/odig/facerecognition/recortadas"):
      split = path.split('.', 1)[0]
      image = load_image("/home/odig/facerecognition/recortadas/" + path)
      cv2.imshow("Adding faces to traning set...", image)
      cv2.waitKey(50)
      images.append(np.asarray(image, dtype=np.uint8))
      labels.append(split)

    labels = np.asarray(labels, dtype=np.int32)

    model.train(images, labels)
    prediction = model.predict(resized)
    return prediction


if __name__ == "__main__":
  print predict(sys.argv[1])
