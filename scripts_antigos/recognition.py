import cv2
import os
import re
import numpy as np
import sys
import json


images = []
labels = []


def load_image(path):
    return cv2.imread(path, cv2.IMREAD_GRAYSCALE)

for path in os.listdir("/home/odig/scripts/recortadas"):
    split = path.split('.', 1)[0]
    image = load_image("/home/odig/scripts/recortadas/" + path)

    images.append(np.asarray(image, dtype=np.uint8))
    labels.append(split)


labels = np.asarray(labels, dtype=np.int32)
faceRecognizer = cv2.createEigenFaceRecognizer();
faceRecognizer.train(np.asarray(images), labels)


#Load file to predict
prediction_image = load_image(sys.argv[1])

# Create the haar cascade
faceCascade = cv2.CascadeClassifier('data/haarcascade_frontalface_alt.xml')

# Read the image
image = cv2.imread(sys.argv[1])

gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

# Detect faces in the image
faces = faceCascade.detectMultiScale(
    gray,
    scaleFactor=1.3,
    minNeighbors=5,
    minSize=(30, 30)
)

print("{0} rostos detectados!".format(len(faces)))

#prediction = faceRecognizer.predict(prediction_image)

#print(json.dumps(prediction))