import cv2
import os
import re
import numpy as np
import sys
import json

def detect(img, cascade):
  gray = to_grayscale(img)
  rects = cascade.detectMultiScale(gray, scaleFactor=1.05, minNeighbors=5, minSize=(30, 30))

  if len(rects) == 0:
    return []
  return rects


def load_image(path):
    return cv2.imread(path, cv2.IMREAD_GRAYSCALE)

def detect_faces(img):
  cascade = cv2.CascadeClassifier("data/haarcascade_frontalface_alt.xml")
  image = cv2.imread(img)

  return detect(image, cascade)

def to_grayscale(img):
  gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
  gray = cv2.equalizeHist(gray)
  return gray

def contains_face(img):
  return len(detect_faces(img)) > 0

def save(path, img):
  cv2.imwrite(path, img)

def crop_faces(img, faces):
  for face in faces:
    x, y, h, w = [result for result in face]
    return img[y:y+h,x:x+w]

def new():

  for path in os.listdir("/home/odig/facerecognition/exemplos"):
    split = path.split('.', 1)[0]
    faces = detect_faces("/home/odig/facerecognition/exemplos/" + path)

    if len(faces) > 0:
      image = cv2.imread("/home/odig/facerecognition/exemplos/" + path)

      cropped = to_grayscale(crop_faces(image, faces))
      resized = cv2.resize(cropped, (200,200))
      save("/home/odig/facerecognition/recortadas/"+path, resized)
      print "Save " + path
      #cv2.imshow("Rostos encontrados", resized)
      #cv2.waitKey(0)
    else :
      print "No faces"


if __name__ == "__main__":
  new()
